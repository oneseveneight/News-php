# News.php
![News.php](news.php.png)

Google news for Gemini

Live at gemini://illegaldrugs.net/cgi-bin/news.php


## Requirements
- PHP 7 (at least thats what I used)
- A gemini server that supports CGI
- A [reader.php](https://tildegit.org/sose/reader.php) you can use

## Setup
- Put it in your cgi-bin
- Set up [reader.php](https://tildegit.org/sose/reader.php) in the same cgi-bin
- If reader.php is not in the same cgi-bin or is a remote path, you will need to change the link at the top of the script

## Notes
- I had no idea how to write php before I started this project and I didn't bother to learn, I kinda just winged it and I think that shows in the code

(c) oneseveneight/sose
