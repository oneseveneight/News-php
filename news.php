#!/usr/bin/php
<?php

$READER_LINK = dirname(__PATH__) . "/reader";
# ^^^^^^^  uri path to an instance of reader.php
# the default is relative, and assumes you have one in the same directory as
# this script

$topics = array("WORLD",
    "BUSINESS",
    "TECHNOLOGY",
    "ENTERTAINMENT",
    "SPORTS",
    "SCIENCE",
    "HEALTH");

$feed_title = "";
$feed = new XMLReader;
if ( getenv("PATH_INFO") !== null ) {
    $feed->open("https://news.google.com/atom"
            . getenv("PATH_INFO")
            . "?"
            . getenv("QUERY_STRING"), "utf-8");
} else {
    $feed->open("https://news.google.com/atom?"
            . getenv("QUERY_STRING"), "utf-8");
}
$feed->read();

echo "20 text/gemini\n";
echo "```\n";
echo "    _   _________       _______   ____  __  ______  \n";
echo "   / | / / ____/ |     / / ___/  / __ \/ / / / __ \ \n";
echo "  /  |/ / __/  | | /| / /\__ \  / /_/ / /_/ / /_/ / \n";
echo " / /|  / /___  | |/ |/ /___/ / / ____/ __  / ____/  \n";
echo "/_/ |_/_____/  |__/|__//____(_)_/   /_/ /_/_/       \n";
echo "====================================================\n";
echo "                  \"Yeah it's news\"                \n";
echo "====================================================\n";
echo "```\n";
foreach ($topics as $topic) {
    echo "=> "
        . getenv("SCRIPT_NAME")
        . "/headlines/section/topic/"
        . $topic
        . " "
        . $topic
        . "\n";
}
while ($feed->read()) {
    if ($feed->name === "title" && $feed_title === "") {
        $feed_title = $feed->readInnerXml();
        $feed_title = str_replace(" - Google News", "", $feed_title);
        echo "\n# " . $feed_title . "\n";
    }

    if ( $feed->name === "entry") {
        $entries= $feed->expand();
        foreach ($entries->childNodes as $entry_node) {
            $multi_source = true;
            if ( $entry_node->tagName === "title" ) {
                echo "\n## " . $entry_node->nodeValue . "\n";
            }

            if ($entry_node->tagName === "content" &&
                    $entry_node->childNodes->length === 1) {
                $sources_html = $entry_node->childNodes->item(0)->data;
                $sources = @DOMDocument::loadHTML($sources_html);
                $sources = $sources->getElementsByTagName("ol");
                if ($sources->length === 0) {
                    $sources = DOMDocument::loadHTML($entry_node
                            ->childNodes
                            ->item(0)
                            ->data);

                    $sources = $sources->getElementsByTagName("a");
                    $multi_source = false;
                }

                if ($multi_source === true) {
                    $sources = $sources->item(0);
                    for ($i = 0; $i < ($sources->childNodes->length -1); $i++) {
                        $source_node = $sources->childNodes[$i];
                        $source_publication = "";
                        $source_link = "";
                        $source_title = "";
                        $anchor_tag = $source_node
                            ->getElementsByTagName("a")
                            ->item(0);

                        $font_tag = $source_node
                            ->getElementsByTagName("font")
                            ->item(0);

                        if ($anchor_tag !== null) {
                            $source_link = $anchor_tag
                                ->attributes
                                ->getNamedItem("href")
                                ->value;

                            $source_title = $anchor_tag->nodeValue;
                            $source_title = utf8_decode($source_title);
                        }
                        if ($font_tag !== null ) {
                            $source_publication = $font_tag->nodeValue;
                        }

                        echo "=> "
                            . $READER_LINK
                            . "?"
                            . urlencode($source_link)
                            . " "
                            . $source_title
                            . " - "
                            . $source_publication
                            . "\n\n";
                    }
                } else {
                    $source_publication = "";
                    $source_link = "";
                    $source_title = "";
                    $anchor_tag = $sources->item(0);
                    $font_tag = $sources->item(1);

                    if ($anchor_tag !== null) {
                        $source_link = $anchor_tag
                            ->attributes
                            ->getNamedItem("href")
                            ->value;

                        $source_title = $anchor_tag->nodeValue;
                    }
                    if ($font_tag !== null ) {
                        $source_publication = $font_tag->nodeValue;
                    }

                    echo "=> "
                        . $source_link
                        . " "
                        . $source_title
                        . " - "
                        . $source_publication
                        . "\n";
                }


            }
        }
    }
}

$feed->close();
